package com.example.ragis_form_mate_ui.Repogetry

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.widget.Toast
import com.example.ragis_form_mate_ui.Activity.Student


private const val DB_NAME="ChetuDB"
private const val VERSION=4
private const val TABLE_NAME="Student"
private const val FNAME="fname"
private const val LASTNAME="lnaame"
private const val MONU="Mo_number"
private const val ALTER_MOBILE="AL_MO"
private const val E_Mail="email"
private const val DOB="dob"
private const val GENDER="gender"
private const val QYARY="quary"
private const val SRNU="sr"
private const val USERTYPE="user"


class RepogetryDataBase( private val context: Context) {

    val quary="CREATE TABLE $TABLE_NAME ($SRNU INTEGER PRIMARY KEY AUTOINCREMENT,$FNAME TEXT,$LASTNAME TEXT,$MONU TEXT,$ALTER_MOBILE TEXT,$E_Mail TEXT,$DOB TEXT, $GENDER TEXT ,$QYARY TEXT,$USERTYPE TEXT)"
    val dbhelper=Dbhelper(context)
    val sqlitedbase=dbhelper.writableDatabase
    fun Svaedata(Fname:String,Lastname:String,MobilNu:String,Alternet:String,Email:String,gender:String,Quary:String,Usertype:String){
        var contenrvalue=ContentValues()
        contenrvalue.put(FNAME,Fname)
        contenrvalue.put(LASTNAME,Lastname)
        contenrvalue.put(MONU,MobilNu)
        contenrvalue.put(ALTER_MOBILE,Alternet)
        contenrvalue.put(E_Mail,Email)
        contenrvalue.put(GENDER,gender)
        contenrvalue.put(QYARY,Quary)
        contenrvalue.put(USERTYPE,Usertype)

      //  contenrvalue.put(DOB,Dob)
        var id:Long= sqlitedbase.insert(TABLE_NAME,null,contenrvalue)
        if(id>0){
              Toast.makeText(context, "Data Saved Successfully", Toast.LENGTH_SHORT).show()
          }
          else
          {
              Toast.makeText(context, "Some Thing Went wrong", Toast.LENGTH_SHORT).show()
          }
    }
    fun getAlldata():ArrayList<Student>{
        var arraylofdata=ArrayList<Student>()
        val array= arrayOf(SRNU, FNAME, LASTNAME, MONU, ALTER_MOBILE, E_Mail, GENDER, QYARY,
            USERTYPE)
        val cursor= sqlitedbase.query(TABLE_NAME,array,null,null,null,null,null)
        if(cursor.count>0){
            cursor.moveToFirst()
           do {


               val srnum = cursor.getString(0)
               val fname = cursor.getString(1)
               val lastname = cursor.getString(2)
               val mobnumber = cursor.getString(3)
               val altermub = cursor.getString(4)
               val email = cursor.getString(5)
               val gender = cursor.getString(6)
               val quary = cursor.getString(7)
               val user=cursor.getString(8)
               val student = Student(srnum, fname, lastname, mobnumber, altermub, email, gender, quary,user)
               arraylofdata.add(student)

           } while (cursor.moveToNext())


        }
        else{
                Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show()
        }
        return arraylofdata
    }
    fun deletesinglerecord(rowId:String){
        val id=sqlitedbase.delete(TABLE_NAME, "$SRNU==$rowId",null)
        if(id>0){
            Toast.makeText(context,"Delete Successfully", Toast.LENGTH_SHORT).show()
        }
        else{
            Toast.makeText(context, "Some thing went wrong", Toast.LENGTH_SHORT).show()

        }


    }
    fun deletesingldata():Cursor{
        val array= arrayOf(SRNU, FNAME, LASTNAME, MONU, ALTER_MOBILE, E_Mail, GENDER, QYARY,
            USERTYPE)
        return sqlitedbase.query(TABLE_NAME,array,null,null,null,null,null)

    }
    fun updaterecord(Fname:String,Lastname:String,MobilNu:String,Alternet:String,Email:String,gender:String,Quary:String,rowId: String,usertype:String){
        var contenrvalue=ContentValues()
        contenrvalue.put(FNAME,Fname)
        contenrvalue.put(LASTNAME,Lastname)
        contenrvalue.put(MONU,MobilNu)
        contenrvalue.put(ALTER_MOBILE,Alternet)
        contenrvalue.put(E_Mail,Email)
        contenrvalue.put(GENDER,gender)
        contenrvalue.put(QYARY,Quary)
        contenrvalue.put(USERTYPE,usertype)
        //  contenrvalue.put(DOB,Dob)
        var id=sqlitedbase.update(TABLE_NAME,contenrvalue,"$SRNU==$rowId",null)
        if(id>0){
            Toast.makeText(context,"Successfully  Updated", Toast.LENGTH_SHORT).show()
        }
        else{
            Toast.makeText(context, "Some thing went wrong", Toast.LENGTH_SHORT).show()

        }
    }

     inner class Dbhelper(private val context:Context):SQLiteOpenHelper(context, DB_NAME,null,VERSION){
         override fun onCreate(sqlitedbase: SQLiteDatabase?) {
            sqlitedbase?.execSQL(quary)
         }

         override fun onUpgrade(sqlite: SQLiteDatabase?, Oldversion: Int,newviersion: Int) {
             TODO("Not yet implemented")
         }



     }
}