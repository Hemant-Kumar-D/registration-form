package com.example.ragis_form_mate_ui.Activity

import android.app.ActionBar
import android.app.Dialog
import android.content.Intent
import android.database.Cursor
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.ContextMenu
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.RadioButton
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.databinding.adapters.LinearLayoutBindingAdapter
import androidx.lifecycle.ViewModelProvider
import com.example.ragis_form_mate_ui.Factory.MainActivityRepoFActory
import com.example.ragis_form_mate_ui.R
import com.example.ragis_form_mate_ui.Repogetry.*
import com.example.ragis_form_mate_ui.ViewModel.ViewModelMainActivity
import com.example.ragis_form_mate_ui.databinding.ActivityMainBinding
import com.example.ragis_form_mate_ui.databinding.ActivityViewInformationBinding

class viewInformation : AppCompatActivity(), AdapterView.OnItemLongClickListener {
    private lateinit var binding: ActivityViewInformationBinding
    lateinit var factory: MainActivityRepoFActory
    lateinit var viewmodel: ViewModelMainActivity
    private var position = 0
    private lateinit var cursor: Cursor

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_view_information)
        factory = MainActivityRepoFActory(RepogetryDataBase(this))
        viewmodel = ViewModelProvider(this, factory)[ViewModelMainActivity::class.java]
        registerForContextMenu(binding.listview)
        binding.listview.setOnItemLongClickListener(this)
     setlistview()

    }

    override fun onItemLongClick(
        parent: AdapterView<*>?,
        view: View?,
        position: Int,
        id: Long
    ): Boolean {
        this.position = position
        return false
    }

    override fun onCreateContextMenu(
        menu: ContextMenu?,
        v: View?,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        val inflater = menuInflater
        inflater.inflate(R.menu.deleteupdate, menu)
        super.onCreateContextMenu(menu, v, menuInfo)
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.delete -> {
                cursor.moveToPosition(position)
                var id = cursor.getString(0)
                viewmodel.deletesinglerecord(id)
                setlistview()


            }
            R.id.update -> {
               // val binding=ActivityMainBinding.this,ActivityMainBinding::class.java)
                cursor.moveToPosition(position)
                val srnum = cursor.getString(0)
                val fname = cursor.getString(1)
                val lastname = cursor.getString(2)
                val mobnumber = cursor.getString(3)
                val altermub = cursor.getString(4)
                val email = cursor.getString(5)
                val gender = cursor.getString(6)
                val quary = cursor.getString(7)
                val usertype=cursor.getString(8)
                var  genders=gender
                val layoutcustombinding=ActivityMainBinding.inflate(layoutInflater)
                 val dialog=Dialog(this)
                dialog.setContentView(layoutcustombinding.root)
                dialog.setCancelable(false)
                val windowManager=WindowManager.LayoutParams()
                windowManager.width=ActionBar.LayoutParams.MATCH_PARENT
                windowManager.height=ActionBar.LayoutParams.WRAP_CONTENT
                dialog.window?.attributes=windowManager
                dialog.show()
                if(genders.equals("male",true)){
                    layoutcustombinding.rbMalename.isChecked=true
                }
               else if(genders.equals("female",true)){
                    layoutcustombinding.rbFemalename.isChecked=true
                }
                else{
                    layoutcustombinding.rbTranGender.isChecked=true
                }
                layoutcustombinding.fname.setText(fname)
                layoutcustombinding.lastname.setText(lastname)
                layoutcustombinding.mobilenumber2.setText(mobnumber)
                layoutcustombinding.alternetmobile2.setText(altermub)
                layoutcustombinding.email2.setText(email)
                layoutcustombinding.usertype.setText(usertype)
                layoutcustombinding.rdGroup.setOnCheckedChangeListener { radioGroup, i ->
                    val id =radioGroup.checkedRadioButtonId
                    when(id){
                        R.id.rb_malename ->{
                            genders=layoutcustombinding.rbMalename.text.toString()

                        }
                        R.id.rb_femalename ->{
                            genders=layoutcustombinding.rbFemalename.text.toString()
                        }
                        R.id.rb_tran_gender->{
                            genders=layoutcustombinding.rbTranGender.text.toString()

                        }
                    }
                }






        layoutcustombinding.rbSubmit.text="UPDATE"
        layoutcustombinding.rbSubmit.setOnClickListener {
            viewmodel.updatedata(layoutcustombinding.editFname.editText?.text.toString(),layoutcustombinding.editLastname.editText?.text.toString(),layoutcustombinding.editMoNumber.editText?.text.toString()
                ,layoutcustombinding.editAlmoNumber.editText?.text.toString(),layoutcustombinding.editEmail.editText?.text.toString(),genders!!,quary,srnum,usertype)
            setlistview()
            dialog.dismiss()
        }
//


            }
        }
        return super.onContextItemSelected(item)

    }

    private fun getalldata(): ArrayList<Student> {
        cursor = viewmodel.getalldata()
        var arraylofdata = ArrayList<Student>()
        // val array= arrayOf(SRNU, FNAME, LASTNAME, MONU, ALTER_MOBILE, E_Mail, GENDER, QYARY)
        //val cursor= sqlitedbase.query(TABLE_NAME,array,null,null,null,null,null)
        if (cursor.count > 0) {
            cursor.moveToFirst()
            do {


                val srnum = cursor.getString(0)
                val fname = cursor.getString(1)
                val lastname = cursor.getString(2)
                val mobnumber = cursor.getString(3)
                val altermub = cursor.getString(4)
                val email = cursor.getString(5)
                val gender = cursor.getString(6)
                val quary = cursor.getString(7)
                val usertype=cursor.getString(8)
                val student =
                    Student(srnum, fname, lastname, mobnumber, altermub, email, gender, quary,usertype)
                arraylofdata.add(student)

            }
            while (cursor.moveToNext())


        } else {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show()
        }
    return arraylofdata
    }
    fun setlistview() {
        val dataofstudent = getalldata()
        val myadapter = arrayAdapter(dataofstudent)
        binding.listview.adapter = myadapter
    }
}