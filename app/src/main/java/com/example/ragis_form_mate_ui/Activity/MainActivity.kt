package com.example.ragis_form_mate_ui.Activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.CompoundButton
import android.widget.RadioGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.ragis_form_mate_ui.Factory.MainActivityRepoFActory
import com.example.ragis_form_mate_ui.R
import com.example.ragis_form_mate_ui.Repogetry.RepogetryDataBase
import com.example.ragis_form_mate_ui.ViewModel.ViewModelMainActivity
import com.example.ragis_form_mate_ui.databinding.ActivityMainBinding
import java.text.SimpleDateFormat
import java.util.Calendar


class MainActivity : AppCompatActivity(), RadioGroup.OnCheckedChangeListener,
    CompoundButton.OnCheckedChangeListener, View.OnClickListener {
    private lateinit var binding:ActivityMainBinding // object hai xml file jo sari id ko hold kerke rakhta ha
    lateinit var factory:MainActivityRepoFActory
    lateinit var viewmodel:ViewModelMainActivity
    var gender:String?= null
    var arraylist:ArrayList<String> = ArrayList()
  //  lateinit var usertype:String

  //  var isCheked:Boolean=false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       // binding= ActivityMainBinding.inflate(layoutInflater)
       binding=DataBindingUtil.setContentView(this, R.layout.activity_main)

         factory= MainActivityRepoFActory(RepogetryDataBase(this))
        viewmodel=ViewModelProvider(this,factory)[ViewModelMainActivity::class.java]


        binding.rdGroup.setOnCheckedChangeListener(this)
        binding.cbCoding.setOnCheckedChangeListener(this)
        binding.cbGaming.setOnCheckedChangeListener(this)
        binding.cbReadingNobel.setOnCheckedChangeListener(this)
        binding.cbTavling.setOnCheckedChangeListener(this)
        binding.cbPlayCricket.setOnCheckedChangeListener(this)

        binding.rbSubmit.setOnClickListener(this)
        val studentdata=viewmodel.getdata()

       Log.d("saksajsja","$studentdata")
        val intent=intent
//        val usertypeskip=intent.getStringExtra("typecast")
       val usertype= intent.getStringExtra(keys.UserType)
        //Toast.makeText(this, "$usertype", Toast.LENGTH_SHORT).show()
         binding.usertype.text=usertype
        val date =gettime()
        binding.datetime.text=date



        // var gender:String=""
        }
    override fun onClick(p0: View?) {
        when(p0?.id){
            R.id.rb_submit ->{
                if(binding.editFname.editText!!.text.isEmpty()&&binding.editLastname.editText!!.text.isEmpty()&&binding.editMoNumber.editText!!.text.isEmpty()&&binding.
                    editAlmoNumber.editText!!.text.isEmpty()&&binding.editEmail.editText!!.text.isEmpty()){
                    Toast.makeText(this,"Please fill all information",Toast.LENGTH_LONG).show()
                }
                else if(binding.editFname.editText!!.text.isEmpty()){
                    Toast.makeText(this,"Please enter Firstname",Toast.LENGTH_LONG).show()
                    binding.editFname.requestFocus()

                }
                else if(binding.editLastname.editText!!.text.isEmpty()){
                    Toast.makeText(this,"Please enter Lastname",Toast.LENGTH_LONG).show()
                    binding.editLastname.requestFocus()

                }
                else if(binding.editMoNumber.editText!!.text.isEmpty()){
                    Toast.makeText(this,"Please enter Mo.number",Toast.LENGTH_LONG).show()
                    binding.editMoNumber.requestFocus()

                }
                else if(binding.editAlmoNumber.editText!!.text.isEmpty()){
                    Toast.makeText(this,"Please enter Aler.mo.number",Toast.LENGTH_LONG).show()
                    binding.editAlmoNumber.requestFocus()

                }
//                if(!Patterns.EMAIL_ADDRESS.matcher(editTextEmail.getText().toString()).matches()){ //show toast:enter valid email } –
//                    AG-Developer
//                    Dec 2, 2022 at 19:08
                else if(binding.editEmail.editText!!.text.isEmpty()){
                    Toast.makeText(this,"Please enter Email",Toast.LENGTH_LONG).show()
                    binding.editEmail.requestFocus()
                }
                else if(binding.editMoNumber.editText?.text.toString().equals(binding.editAlmoNumber.editText!!.text.toString())){
                    Toast.makeText(this,"plese fill diffrent number",Toast.LENGTH_LONG).show()
                }
                else if(!Patterns.EMAIL_ADDRESS.matcher(binding.editEmail.editText!!.text).matches()){
                    Toast.makeText(this, "please fill wright mail", Toast.LENGTH_SHORT).show()
                }

                else
                {

                    val listname=arraylist.toString().replace("["," ").replace("]"," ")
                    val usertype= intent.getStringExtra(keys.UserType)
                   viewmodel.Inasertdata(binding.editFname.editText?.text.toString(),binding.editLastname.editText?.text.toString(),binding.editMoNumber.editText?.text.toString()
                         ,binding.editAlmoNumber.editText?.text.toString(),binding.editEmail.editText?.text.toString(),gender!!,listname,usertype!!)
   //girjdskjcskjh
                    finish();
                    startActivity(getIntent())
                }


            }

        }
    }




    override fun onCheckedChanged(chacked: RadioGroup?, p1: Int) {

       when(chacked?.checkedRadioButtonId){
           R.id.rb_malename ->{
               gender = binding.rbMalename.text.toString()
           }
           R.id.rb_femalename ->{
               gender=binding.rbFemalename.text.toString()
           }
           R.id.rb_tran_gender ->{
               gender=binding.rbTranGender.text.toString()
           }
       }
    }

    override fun onCheckedChanged(view: CompoundButton?, isCheked: Boolean) {
        when(view?.id){
            R.id.cb_coding ->{
                if(binding.cbCoding.isChecked){
                    arraylist.add(binding.cbCoding.text.toString())

                }
                else
                {
                  arraylist.remove(binding.cbCoding.text.toString())
                }

            }
            R.id.cb_gaming ->{
                if(binding.cbGaming.isChecked){
                    arraylist.add(binding.cbGaming.text.toString())

                }
                else
                {
                    arraylist.remove(binding.cbGaming.text.toString())

                }

            }
            R.id.cb_tavling ->{
                if(binding.cbTavling.isChecked){
                    arraylist.add(binding.cbTavling.text.toString())

                }
                else
                {
                    arraylist.remove(binding.cbTavling.text.toString())

                }

            }
            R.id.cb_reading_nobel ->{
                if (binding.cbReadingNobel.isChecked) {
                    arraylist.add(binding.cbReadingNobel.text.toString())

                } else {
                    arraylist.remove(binding.cbReadingNobel.text.toString())


                }

            }
            R.id.cb_play_cricket ->{
                if(binding.cbPlayCricket.isChecked){
                    arraylist.add(binding.cbPlayCricket.text.toString())

                }
                else
                {
                    arraylist.remove(binding.cbPlayCricket.text.toString())

                }

            }
        }

    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menuitem, menu)
        return super.onCreateOptionsMenu(menu)
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.menu_logout ->{

            }
            R.id.user_information ->{
                startActivity(Intent(this, viewInformation::class.java))
            }
        }
        return true
    }
    private fun gettime():String{
        val date=Calendar.getInstance().time
        val farmetor=SimpleDateFormat.getInstance()
        return farmetor.format(date).toString()
    }




}


