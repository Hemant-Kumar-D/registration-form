package com.example.ragis_form_mate_ui.Activity

import android.content.Intent

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.view.get
import androidx.lifecycle.ViewModelProvider
import com.example.ragis_form_mate_ui.Factory.viewmodelfactory
import com.example.ragis_form_mate_ui.R
import com.example.ragis_form_mate_ui.Repogetry.sharedprefrence
import com.example.ragis_form_mate_ui.ViewModel.sharedprefrenceviewmodel
import com.example.ragis_form_mate_ui.databinding.ActivityLoginBinding

class LoginActivity : AppCompatActivity(), View.OnClickListener,
    AdapterView.OnItemSelectedListener {
     private lateinit var binding:ActivityLoginBinding
     lateinit var viewModel: sharedprefrenceviewmodel
     lateinit var factory: viewmodelfactory
     var data:String=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        factory= viewmodelfactory(sharedprefrence,this)
        viewModel = ViewModelProvider(this,factory)[sharedprefrenceviewmodel::class.java]
        val array= arrayOf("Guest","HR","Consaltent","Faculty")
        val spineradpter=ArrayAdapter(this,android.R.layout.simple_expandable_list_item_1,array)
      binding.spineerData.adapter=spineradpter
        binding.spineerData.onItemSelectedListener= this



        binding.textSignin.setOnClickListener(this)
        binding.btnLogin.setOnClickListener(this)


    }

    override fun onClick(p0: View?) {
        when(p0?.id){
            R.id.text_signin ->{



                startActivity(Intent(this, SignActivity::class.java))
            }
            R.id.btn_login ->{

              if(binding.editFname.text.toString().isEmpty()&&binding.editLastname.text.toString()
                      .isEmpty()&&binding.editMonumber.text.toString().isEmpty()){
                  Toast.makeText(this, "Please fill value", Toast.LENGTH_SHORT).show()

              }else if(binding.editFname.text.toString().isEmpty()){
                  Toast.makeText(this, "Please fill firstname", Toast.LENGTH_SHORT).show()

              }
                else if(binding.editLastname.text.toString().isEmpty()){
                  Toast.makeText(this, "Please fill Lastname", Toast.LENGTH_SHORT).show()

              }else if(binding.editMonumber.text.toString().isEmpty()){
                  Toast.makeText(this, "Please fill Mobile Number", Toast.LENGTH_SHORT).show()

              }
                else{
                  Toast.makeText(this, "succesfully ragisterd", Toast.LENGTH_SHORT).show()
                  viewModel.savedata(binding.editFname.text.toString(),binding.editLastname.text.toString(),
                      binding.editMonumber.text.toString(),data)

                  startActivity(Intent(this, SignActivity::class.java))
              }



            }

        }

    }

    override fun onItemSelected(adapter: AdapterView<*>?, view: View?, position: Int, p3: Long) {
        data= adapter?.getItemAtPosition(position).toString()
        Toast.makeText(this, "$data", Toast.LENGTH_SHORT).show()
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        TODO("Not yet implemented")
    }
}