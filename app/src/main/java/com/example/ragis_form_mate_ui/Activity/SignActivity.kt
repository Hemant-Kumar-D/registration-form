package com.example.ragis_form_mate_ui.Activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.ragis_form_mate_ui.Factory.viewmodelfactory
import com.example.ragis_form_mate_ui.R
import com.example.ragis_form_mate_ui.Repogetry.sharedprefrence
import com.example.ragis_form_mate_ui.ViewModel.sharedprefrenceviewmodel
import com.example.ragis_form_mate_ui.databinding.ActivitySignBinding

class SignActivity : AppCompatActivity() {
    private lateinit var binding:ActivitySignBinding
     lateinit var viewmodel: sharedprefrenceviewmodel
     lateinit var factory: viewmodelfactory
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding=DataBindingUtil.setContentView(this, R.layout.activity_sign)
        factory= viewmodelfactory(sharedprefrence,this)
        viewmodel=ViewModelProvider(this,factory)[sharedprefrenceviewmodel::class.java]
        val mobile=viewmodel.getdata()





        binding.btnSingLogin.setOnClickListener {


      if(binding.editMoSignActivity.editText?.text.toString().equals(mobile)){
           val usertype=viewmodel.getUsertype()
          val intent=Intent(this, MainActivity::class.java)
          intent.putExtra(keys.UserType,usertype)
          startActivity(intent)

      }
            else{
          Toast.makeText(this, "Invalid Number", Toast.LENGTH_SHORT).show()

            }




        }
        binding.textSignup.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
        }
        binding.skip.setOnClickListener {
            var type=viewmodel.getUsertype()
            startActivity(Intent(this, MainActivity::class.java))
            intent.putExtra(type,"typecast")

        }



    }

}