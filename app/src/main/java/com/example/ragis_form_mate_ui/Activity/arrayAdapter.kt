package com.example.ragis_form_mate_ui.Activity

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.databinding.DataBindingUtil
import com.example.ragis_form_mate_ui.R
import com.example.ragis_form_mate_ui.databinding.ActivityViewInformationBinding
import com.example.ragis_form_mate_ui.databinding.UserdetailsBinding

class arrayAdapter(private val arraylist:ArrayList<Student>):BaseAdapter() {
    override fun getCount(): Int {
         return arraylist.count()
    }

    override fun getItem(position: Int): Any {
        return arraylist[position]
    }

    override fun getItemId(position: Int): Long {
       // TODO("Not yet implemented")
        return  arraylist[position].hashCode().toLong()

    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

var view=convertView
        view=LayoutInflater.from(parent!!.context).inflate(R.layout.userdetails,parent,false)
        val binding=DataBindingUtil.bind<UserdetailsBinding>(view)
        binding!!.fnameItem.text = arraylist[position].Fname
        binding!!.lastnameItem.text = arraylist[position].Lastname
       binding!!.mobile.text = arraylist[position].Mobilnumber
        binding!!.alernetmobile.text = arraylist[position].Altermo
       binding!!.email.text = arraylist[position].Email
       binding!!.gender.text = arraylist[position].gendr
        binding!!.quary.text = arraylist[position].quary
        binding.usertype.text=arraylist[position].Usertype
        return view





    }
}