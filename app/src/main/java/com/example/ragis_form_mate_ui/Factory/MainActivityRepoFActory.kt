package com.example.ragis_form_mate_ui.Factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.ragis_form_mate_ui.Repogetry.RepogetryDataBase
import com.example.ragis_form_mate_ui.ViewModel.ViewModelMainActivity
import com.example.ragis_form_mate_ui.ViewModel.sharedprefrenceviewmodel

class MainActivityRepoFActory(private var repodatabase: RepogetryDataBase):ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ViewModelMainActivity::class.java)){
            return ViewModelMainActivity(repodatabase) as T
        }
        throw IllegalArgumentException("Unknown class")
    }
}